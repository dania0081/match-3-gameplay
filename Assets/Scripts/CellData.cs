/// <summary>
/// ������ � ������
/// </summary>
public class CellData
{
    public enum CellType : sbyte
    {
        Hole = -1,
        None = 0,
        Apple = 1,
        Banana = 2,
        BlueBerry = 3,
        Grape = 4,
        Pear = 5,
    }

    public CellType cellType;
    public Point point;
    private Cell _cell;
    
    public CellData(CellType cellType, Point point)
    {
        this.cellType = cellType;
        this.point = point;
    }

    /// <summary>
    /// �������� ������
    /// </summary>
    public Cell GetCell()
    {
        return _cell;
    }

    /// <summary>
    /// ������ ����� ������
    /// </summary>
    /// <param name="newCell">����� ������</param>
    public void SetCell(Cell newCell)
    {
        _cell = newCell;
        if(_cell == null)
        {
            cellType = CellType.None;
        }
        else
        {
            cellType = newCell.CellType;
            _cell.SetCellPoint(point);
        }
    }
}
