using StaticData;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ������� �� �������� �����
/// </summary>
public class CellFactory : MonoBehaviour
{
    private BoardService _boardService;
    private readonly List<FallMatchedCell> _fallMatchedCells = new List<FallMatchedCell>();

    [Header("BoardRects")]
    [SerializeField] private RectTransform _boardRect;
    [SerializeField] private RectTransform _fallMatchedBoardRect;

    [Header("Prefabs")]
    [SerializeField] private Cell _cellPrefab;
    [SerializeField] private FallMatchedCell _fallMatchedCellPrefab;

    /// <summary>
    /// ��������������� �����
    /// </summary>
    public void InstantiateBoard(BoardService boardService, CellMover cellMover)
    {
        _boardService = boardService;
        for (int y = 0; y < Config.BoardHeight; y++)
        {
            for (int x = 0; x < Config.BoardWidth; x++)
            {
                var point = new Point(x, y);
                var cellData = boardService.GetCellDataAtPoint(new Point(x, y));
                var cellType = cellData.cellType;

                if (cellType <= 0)
                {
                    continue;
                }

                Cell cell = InstantiateCell();
                cell.rect.anchoredPosition = BoardService.GetBoardPositionFromPoint(point);
                cell.Initialize(
                    new CellData(cellType, new Point(x, y)),
                    boardService.CellSprites[(int)(cellType - 1)],
                    cellMover
                    );
                cellData.SetCell(cell);
            }
        }
    }

    /// <summary>
    /// ��������������� ������
    /// </summary>
    public Cell InstantiateCell()
    {
        return Instantiate(_cellPrefab, _boardRect);
    }

    /// <summary>
    /// ��������� �������� ������� ��� �����
    /// </summary>
    public void MatchedCellAnim(Point point)
    {
        var available = new List<FallMatchedCell>();
        foreach (var fallMatchedCell in _fallMatchedCells)
        {
            if (!fallMatchedCell.isFalling)
            {
                available.Add(fallMatchedCell);
            }
        }
        FallMatchedCell showedFallMatchedCell;
        if (available.Count > 0)
        {
            showedFallMatchedCell = available[0];
        }
        else
        {
            var fallMatchedCell = Instantiate(_fallMatchedCellPrefab, _fallMatchedBoardRect);
            showedFallMatchedCell = fallMatchedCell;
            _fallMatchedCells.Add(fallMatchedCell);
        }

        var cellTypeIndex = (int)_boardService.GetCellTypeAtPoint(point) - 1;
        if (showedFallMatchedCell != null && cellTypeIndex >= 0 && cellTypeIndex < _boardService.CellSprites.Length)
        {
            showedFallMatchedCell.Initialize(
                _boardService.CellSprites[cellTypeIndex],
                BoardService.GetBoardPositionFromPoint(point)
                );
        }
    }
}
