﻿using StaticData;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Обрабатывает поведение падающих ячеек (Анимация)
/// </summary>
public class FallMatchedCell : MonoBehaviour
{
    [HideInInspector] public bool isFalling;

    [SerializeField] private float _speed = 16f;
    [SerializeField] private float _gravity = 32f;
    [SerializeField] private RectTransform _rect;
    [SerializeField] private Image _image;
    private Vector2 _moveDirection;
    private Vector3 _viewportPosition;
    /// <summary>
    /// Значение смещения, чтобы выйти за границу камеры
    /// </summary>
    /// <remarks>
    /// Смещаем границу на 1.5 размера ячейки (с запасом)
    /// </remarks>
    private float _offsetToInvis = Config.CellSize / 2 / 1000f;
    
    public void Initialize(Sprite sprite, Vector2 startPosition)
    {
        isFalling = true;

        _moveDirection = Vector2.up;
        _moveDirection.x = Random.Range(-1f, 1f);
        _moveDirection *= _speed / 2;

        _image.sprite = sprite;
        _rect.anchoredPosition = startPosition;

        if (!gameObject.activeInHierarchy)
        {
            gameObject.SetActive(true);
        }
        StartCoroutine(WaitForDeath());
    }

    /// <summary>
    /// Отключаем ячейки через 3 сек
    /// </summary>
    private IEnumerator WaitForDeath()
    {
        yield return new WaitForSeconds(3);
        gameObject.SetActive(false);
    }

    private void Update()
    {
        if (!isFalling)
        {
            return;
        }
        _moveDirection.y -= Time.deltaTime * _gravity;
        _moveDirection.x = Mathf.Lerp(_moveDirection.x, 0, Time.deltaTime);
        _rect.anchoredPosition += _moveDirection * (Time.deltaTime * _speed);
        _viewportPosition = Camera.main.WorldToViewportPoint(transform.position);
        var isOffTheCameraBounds = _viewportPosition.y < 0 - _offsetToInvis ||
                                   _viewportPosition.y > 1 + _offsetToInvis ||
                                   _viewportPosition.x < 0 - _offsetToInvis ||
                                   _viewportPosition.x > 1 + _offsetToInvis;
        if(isOffTheCameraBounds)
        {
            isFalling = false;
        }
    }
}