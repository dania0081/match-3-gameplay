using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// ������ �� ������� - ������
/// </summary>
public class Cell : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public RectTransform rect;
    [SerializeField] private Image _image;

    private CellData _cellData;
    private CellMover _cellMover;
    [SerializeField] private float _moveSpeed = 10f;
    private Vector2 _position;
    private bool _isUpdating;

    public Point Point => _cellData.point;
    public CellData.CellType CellType => _cellData.cellType;

    public void Initialize(CellData cellData, Sprite sprite, CellMover cellMover)
    {
        _cellData = cellData;
        _image.sprite = sprite;
        _cellMover = cellMover;
    }

    /// <summary>
    /// ������������ ������
    /// </summary>
    /// <returns>��������� �� ������ � ��������</returns>
    public bool UpdateCell()
    {
        if (Vector3.Distance(rect.anchoredPosition, _position) > 1)
        {
            MoveToPostion(_position);
            _isUpdating = true;
        }
        else
        {
            rect.anchoredPosition = _position;
            _isUpdating = false;
        }
        return _isUpdating;
    }

    /// <summary>
    /// ��������� ������ �������� ������
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerDown(PointerEventData eventData)
    {
        _cellMover.MoveCell(this);
    }

    /// <summary>
    /// ��������� ��������� �������� ������
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerUp(PointerEventData eventData)
    {
        _cellMover.DropCell();
    }

    /// <summary>
    /// ������� ������������ ������ � ��������� �������
    /// </summary>
    public void MoveToPostion(Vector2 position)
    {
        rect.anchoredPosition = Vector2.Lerp(rect.anchoredPosition, position, Time.deltaTime * _moveSpeed);
    }

    /// <summary>
    /// ������ �������� ������
    /// </summary>
    private void UpdateName()
    {
        transform.name = $"Cell [{Point.x}, {Point.y}]";
    }

    /// <summary>
    /// ���������� ������ � ����������� �������
    /// </summary>
    public void ResetPosition()
    {
        _position = BoardService.GetBoardPositionFromPoint(Point);
    }

    /// <summary>
    /// ��������� ������ � ����������� point
    /// </summary>
    /// <param name="point">����� ��� ��������� ������</param>
    public void SetCellPoint(Point point)
    {
        _cellData.point = point;
        UpdateName();
        ResetPosition();
    }
}
