﻿using StaticData;
using UnityEngine;

/// <summary>
/// Двигает ячейки
/// </summary>
public class CellMover
{
    private Cell _movingCell;
    private Point _newPoint;
    private Vector2 _mouseStartPosition;
    private BoardService _boardService;

    public CellMover(BoardService boardService)
    {
        _boardService = boardService;
    }

    public void Update()
    {
        if (_movingCell == null)
        {
            return;
        }

        var mousePosition = (Vector2)Input.mousePosition - _mouseStartPosition;
        var mouseDirection = mousePosition.normalized;
        var absoluteDirection = new Vector2(Mathf.Abs(mousePosition.x), Mathf.Abs(mousePosition.y)); // Определяем в какую сторону больше двигается. Вертикаль или горизонталь

        _newPoint = Point.Clone(_movingCell.Point);

        if (mousePosition.magnitude > Config.CellSize / 4) // Магнитуда - длина вектора. Расстояние, насколько мы передвинули mousePosition
        {
            Point addPoint;

            if (absoluteDirection.x > absoluteDirection.y) // Движение по горизонтали
            {
                addPoint = new Point(mouseDirection.x > 0 ? 1 : -1, 0);
            }
            else
            {
                addPoint = new Point(0, mouseDirection.y > 0 ? -1 : 1);
            }

            _newPoint.Add(addPoint);
            var newPosition = BoardService.GetBoardPositionFromPoint(_movingCell.Point);
            if (!_newPoint.Equals(_movingCell.Point)) // Передвинули на достаточное расстояние
            {
                newPosition += Point.Multiply(new Point(addPoint.x, -addPoint.y), Config.CellSize / 2).ToVector();
            }
            _movingCell.MoveToPostion(newPosition);
        }
    }

    /// <summary>
    /// Обработка движения ячейки
    /// </summary>
    /// <param name="cell">Ячейка</param>
    public void MoveCell(Cell cell)
    {
        if (_movingCell != null)
        {
            return;
        }
        _movingCell = cell;
        _mouseStartPosition = Input.mousePosition;
    }

    /// <summary>
    /// Обработка окончания движения ячейки
    /// </summary>
    public void DropCell()
    {
        if (_movingCell == null)
        {
            return;
        }
        if (_newPoint.Equals(_movingCell.Point))
        {
            _boardService.ResetCell(_movingCell);
        }
        else
        {
            _boardService.FlipCells(_movingCell.Point, _newPoint, true);
        }
        _movingCell = null;
    }
}