﻿using UnityEngine;

/// <summary>
/// Вспомогательный класс. Vector2, где x и y позиции ячеек в рамках доски.
/// </summary>
public class Point
{
    public int x;
    public int y;

    public Point(int newX, int newY)
    {
        x = newX;
        y = newY;
    }

    /// <summary>
    /// Умножение
    /// </summary>
    public void Multiply(int value)
    {
        x *= value;
        y *= value;
    }

    /// <summary>
    /// Сложение
    /// </summary>
    public void Add(Point point)
    {
        x += point.x;
        y += point.y;
    }

    /// <summary>
    /// Сравнение
    /// </summary>
    public bool Equals(Point point)
        => x == point.x && y == point.y;

    /// <summary>
    /// Преобразование Point -> Vector2
    /// </summary>
    public Vector2 ToVector()
        => new(x, y);

    /// <summary>
    /// Преобразование Vector2 -> Point
    /// </summary>
    public static Point FromVector(Vector2 vector)
        => new((int)vector.x, (int)vector.y);

    /// <summary>
    /// Преобразование Vector3 -> Point
    /// </summary>
    public static Point FromVector(Vector3 vector)
        => new((int)vector.x, (int)vector.y);
    
    /// <summary>
    /// Умножение
    /// </summary>
    public static Point Multiply(Point point, int value)
        => new(point.x * value, point.y * value);

    /// <summary>
    /// Сложение
    /// </summary>
    public static Point Add(Point point1, Point point2)
        => new(point1.x + point2.x, point1.y + point2.y);

    /// <summary>
    /// Клонирование
    /// </summary>
    public static Point Clone(Point point)
        => new(point.x, point.y);
    
    public static Point zero => new Point(0, 0);

    public static Point up => new Point(0, 1);

    public static Point down => new Point(0, -1);

    public static Point left => new Point(-1, 0);

    public static Point right => new Point(1, 0);
}
