using StaticData;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// �������� �� ������ � ������
/// </summary>
[RequireComponent(typeof(CellFactory))]
public class BoardService : MonoBehaviour
{
    public ArrayLayout boardLayout;
    [SerializeField] private Sprite[] _cellSprites;
    [SerializeField] private ParticleSystem _matchFxPrefab;
    [SerializeField] private ScoreService _scoreService;

    public Sprite[] CellSprites => _cellSprites;

    private CellData[,] _board;
    private CellFactory _cellFactory;
    private MatchMachine _matchMachine;
    private CellMover _cellMover;
    
    /// <summary>
    /// ���������� ����� � �������, ������� ����� ���������
    /// </summary>
    private readonly int[] _fillingCellsCountByColumn = new int[Config.BoardWidth];
    
    /// <summary>
    /// ������ � ��������
    /// </summary>
    private readonly List<Cell> _updatingCells = new List<Cell>();
    private readonly List<Cell> _disabledCells = new List<Cell>();
    private readonly List<CellFlip> _flippedCells = new List<CellFlip>();
    private readonly Queue<ParticleSystem> _matchFxs = new Queue<ParticleSystem>();

    private void Awake()
    {
        _cellFactory = GetComponent<CellFactory>();
        _matchMachine = new MatchMachine(this);
        _cellMover = new CellMover(this);
    }

    private void Start()
    {
        InitializeBoard();
        VerifyBoardOnMathes();
        _cellFactory.InstantiateBoard(this, _cellMover);
    }

    private void Update()
    {
        _cellMover.Update();

        var finishedUpdatingCells = new List<Cell>(); // ������, ������� ��������� �����������
        foreach (var cell in _updatingCells)
        {
            if (!cell.UpdateCell())
            {
                finishedUpdatingCells.Add(cell);
            }
        }
        foreach (var cell in finishedUpdatingCells)
        {
            var x = cell.Point.x;
            _fillingCellsCountByColumn[x] = Mathf.Clamp(_fillingCellsCountByColumn[x] - 1, 0, Config.BoardWidth);

            var flip = GetFlip(cell);
            var connectedPoints = _matchMachine.GetMatchedPoints(cell.Point, true);
            Cell flippedCell = null;

            if (flip != null)
            {
                flippedCell = flip.getOtherCell(cell);
                MatchMachine.AddPoints(
                    ref connectedPoints,
                    _matchMachine.GetMatchedPoints(flippedCell.Point, true)
                    );
            }

            if (connectedPoints.Count == 0)
            {
                if (flippedCell != null)
                {
                    FlipCells(cell.Point, flippedCell.Point, false);
                }
            }
            else
            {
                ParticleSystem matchFx;
                if (_matchFxs.Count > 0 && _matchFxs.Peek().isStopped)
                {
                    matchFx = _matchFxs.Dequeue();
                }
                else
                {
                    matchFx = Instantiate(_matchFxPrefab, transform);
                }
                _matchFxs.Enqueue(matchFx);
                matchFx.transform.position = cell.rect.transform.position;
                matchFx.Play();

                foreach (var connectedPoint in connectedPoints)
                {
                    _cellFactory.MatchedCellAnim(connectedPoint);
                    var cellDataAtPoint = GetCellDataAtPoint(connectedPoint);
                    var connectedCell = cellDataAtPoint.GetCell();
                    if (connectedCell != null)
                    {
                        connectedCell.gameObject.SetActive(false);
                        _disabledCells.Add(connectedCell);
                    }
                    cellDataAtPoint.SetCell(null);
                }

                _scoreService.AddScore(connectedPoints.Count);

                ApplyGravityBoard();
            }

            _flippedCells.Remove(flip);
            _updatingCells.Remove(cell);
        }
    }

    /// <summary>
    /// ���������� ��� �����
    /// </summary>
    private void ApplyGravityBoard()
    {
        for (int x = 0; x < Config.BoardWidth; x++)
        {
            for (int y = Config.BoardHeight - 1; y >= 0; y--)
            {
                var point = new Point(x, y);
                var cellData = GetCellDataAtPoint(point);
                var cellTypeAtPoint = GetCellTypeAtPoint(point);

                if (cellTypeAtPoint != 0)
                {
                    continue;
                }

                for (int newY = y - 1; newY >= -1; newY--)
                {
                    var nextPoint = new Point(x, newY);
                    var nextCellType = GetCellTypeAtPoint(nextPoint);
                    if (nextCellType == 0)
                    {
                        continue;
                    }
                    if (nextCellType != CellData.CellType.Hole)
                    {
                        var cellAtPoint = GetCellDataAtPoint(nextPoint);
                        var cell = cellAtPoint.GetCell();

                        cellData.SetCell(cell);
                        _updatingCells.Add(cell);

                        cellAtPoint.SetCell(null);
                    }
                    else
                    {
                        var cellType = GetRandomCellType();
                        var fallPoint = new Point(x, -1 - _fillingCellsCountByColumn[x]);
                        Cell cell;
                        if (_disabledCells.Count > 0)
                        {
                            var revivedCell = _disabledCells[0];
                            revivedCell.gameObject.SetActive(true);
                            cell = revivedCell;
                            _disabledCells.RemoveAt(0);
                        }
                        else
                        {
                            cell = _cellFactory.InstantiateCell();
                        }

                        cell.Initialize(new CellData(cellType, point), _cellSprites[(int)(cellType - 1)], _cellMover);
                        cell.rect.anchoredPosition = GetBoardPositionFromPoint(fallPoint);
                        var holeCell = GetCellDataAtPoint(point);
                        holeCell.SetCell(cell);
                        ResetCell(cell);
                        _fillingCellsCountByColumn[x]++;
                    }
                    break;
                }
            }
        }
    }

    /// <summary>
    /// ������ ������� ��� ������ �� ������� ���� � ��������� �� � ������ ��� ������������ ����������
    /// </summary>
    public void FlipCells(Point firstPoint, Point secondPoint, bool isMainFlipCell)
    {
        if (GetCellTypeAtPoint(firstPoint) < 0)
        {
            return;
        }

        var firstCellData = GetCellDataAtPoint(firstPoint);
        var firstCell = firstCellData.GetCell();
        if (GetCellTypeAtPoint(secondPoint) > 0)
        {
            var secondCellData = GetCellDataAtPoint(secondPoint);
            var secondCell = secondCellData.GetCell();
            firstCellData.SetCell(secondCell);
            secondCellData.SetCell(firstCell);

            if (isMainFlipCell)
            {
                _flippedCells.Add(new CellFlip(firstCell, secondCell));
            }

            _updatingCells.Add(firstCell);
            _updatingCells.Add(secondCell);
        }
        else
        {
            ResetCell(firstCell);
        }
    }

    /// <summary>
    /// ���������� ��� ������� _flippedCells �� ����� �������� ������ � ����
    /// </summary>
    private CellFlip GetFlip(Cell cell)
    {
        foreach (var flip in _flippedCells)
        {
            if (flip.getOtherCell(cell) != null)
            {
                return flip;
            }
        }
        return null;
    }

    /// <summary>
    /// ���������� ������� ������
    /// </summary>
    public void ResetCell(Cell cell)
    {
        cell.ResetPosition();
        _updatingCells.Add(cell);
    }

    /// <summary>
    /// ��������� ����������, ���� ����, �� �������� ������ �� �����
    /// </summary>
    private void VerifyBoardOnMathes()
    {
        for (int y = 0; y < Config.BoardHeight; y++)
        {
            for (int x = 0; x < Config.BoardWidth; x++)
            {
                var point = new Point(x, y);
                var cellTypeAtPoint = GetCellTypeAtPoint(point);

                if (cellTypeAtPoint <= 0)
                {
                    continue;
                }

                var unavailableCellTypes = new List<CellData.CellType>();
                while (_matchMachine.GetMatchedPoints(point, true).Count > 0)
                {
                    if (unavailableCellTypes.Contains(cellTypeAtPoint) == false)
                    {
                        unavailableCellTypes.Add(cellTypeAtPoint);
                    }
                    SetCellTypeAtPoint(point, GetNewCellType(unavailableCellTypes));
                }
            }
        }
    }

    /// <summary>
    /// ���������� ����� ��� ������
    /// </summary>
    private void SetCellTypeAtPoint(Point point, CellData.CellType newCellType)
    {
        _board[point.x, point.y].cellType = newCellType;
    }

    /// <summary>
    /// �������� ����� ��� ������
    /// </summary>
    /// <param name="unavailableCellTypes">����������� ���� �����</param>
    private CellData.CellType GetNewCellType(List<CellData.CellType> unavailableCellTypes)
    {
        var availableCellTypes = new List<CellData.CellType>();
        for (int i = 0; i < CellSprites.Length; i++)
        {
            availableCellTypes.Add((CellData.CellType)i + 1);
        }
        foreach (var unavailableCellType in unavailableCellTypes)
        {
            availableCellTypes.Remove(unavailableCellType);
        }

        return availableCellTypes.Count <= 0
            ? CellData.CellType.None
            : availableCellTypes[Random.Range(0, availableCellTypes.Count)];
    }

    /// <summary>
    /// �������� ��� ������ �� Point
    /// </summary>
    public CellData.CellType GetCellTypeAtPoint(Point point)
    {
        if (point.x < 0 || point.x >= Config.BoardWidth
                        || point.y < 0 || point.y >= Config.BoardHeight)
        {
            return CellData.CellType.Hole;
        }
        return _board[point.x, point.y].cellType;
    }

    /// <summary>
    /// ���������������� �����
    /// </summary>
    /// <remarks>
    /// ���������: ����� ������� � ��� ������
    /// </remarks>
    private void InitializeBoard()
    {
        _board = new CellData[Config.BoardWidth, Config.BoardHeight];
        for (int y = 0; y < Config.BoardHeight; y++)
        {
            for (int x = 0; x < Config.BoardWidth; x++)
            {
                _board[x, y] = new CellData(
                    boardLayout.rows[y].row[x] ? CellData.CellType.Hole : GetRandomCellType(),
                    new Point(x, y)
                    );
            }
        }
    }

    /// <summary>
    /// �������� CellData �� Point
    /// </summary>
    public CellData GetCellDataAtPoint(Point point)
    {
        return _board[point.x, point.y];
    }

    /// <summary>
    /// �������� ��������� (1 - 5) ��� ������
    /// </summary>
    private CellData.CellType GetRandomCellType()
    {
        return (CellData.CellType)(Random.Range(0, _cellSprites.Length) + 1);
    }

    /// <summary>
    /// �������� ������� �� ����� �� Point
    /// </summary>
    public static Vector2 GetBoardPositionFromPoint(Point point)
    {
        return new Vector2(
            Config.CellSize / 2 + Config.CellSize * point.x,
            -Config.CellSize / 2 - Config.CellSize * point.y
            );
    }
}
