namespace StaticData
{
    public class Config
    {
        public const int BoardWidth = 9;
        public const int BoardHeight = 14;
        public const int CellSize = 100;
    }
}
