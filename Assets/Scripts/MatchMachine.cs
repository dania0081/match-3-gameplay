using System.Collections.Generic;

/// <summary>
/// ���� ����� �� �����
/// </summary>
public class MatchMachine
{
    private readonly BoardService _boardService;
    private readonly Point[] _directions =
    {
        Point.up, Point.right, Point.down, Point.left
    };

    public MatchMachine(BoardService boardService)
    {
        _boardService = boardService;
    }

    /// <summary>
    /// ����������� �����.
    /// </summary>
    /// <param name="point">����� ��� �������� �� match</param>
    /// <param name="isFirstInvocation">True, ���� �������� ������ ���, ����� false</param>
    /// <returns>������ ���� �����, ������� �������� ����������.</returns>
    public List<Point> GetMatchedPoints(Point point, bool isFirstInvocation)
    {
        var connectedPoints = new List<Point>();
        var cellTypeAtPoint = _boardService.GetCellTypeAtPoint(point);

        CheckForDirectionMatch(ref connectedPoints, point, cellTypeAtPoint);

        CheckForMiddleMatch(ref connectedPoints, point, cellTypeAtPoint);

        CheckForSquareMatch(ref connectedPoints, point, cellTypeAtPoint);

        if (isFirstInvocation)
        {
            for (int i = 0; i < connectedPoints.Count; i++)
            {
                AddPoints(ref connectedPoints, GetMatchedPoints(connectedPoints[i], false));
            }
        }

        return connectedPoints;
    }

    /// <summary>
    /// ������� ���������� � ��������
    /// </summary>
    private void CheckForSquareMatch(ref List<Point> connectedPoints, Point point, CellData.CellType cellTypeAtPoint)
    {
        for (int i = 0; i < 4; i++)
        {
            var square = new List<Point>();

            var nextCellIndex = i + 1;
            nextCellIndex = nextCellIndex > 3 ? 0 : nextCellIndex;

            Point[] checkPoints =
            {
                Point.Add(point, _directions[i]),
                Point.Add(point, _directions[nextCellIndex]),
                Point.Add(point, Point.Add(_directions[i],_directions[nextCellIndex])),
            };

            foreach (var checkPoint in checkPoints)
            {
                if (_boardService.GetCellTypeAtPoint(checkPoint) == cellTypeAtPoint)
                {
                    square.Add(checkPoint);
                }
            }

            if (square.Count > 2)
            {
                AddPoints(ref connectedPoints, square);
            }
        }
    }

    /// <summary>
    /// ������� ���������� � ��������
    /// </summary>
    /// <remarks>
    /// ���������: ���� ���������� �� ����� �� ������
    /// </remarks>
    private void CheckForMiddleMatch(ref List<Point> connectedPoints, Point point, CellData.CellType cellTypeAtPoint)
    {
        for (int i = 0; i < 2; i++)
        {
            var line = new List<Point>(); // �������� ����� ���������� ����� � ����� �����������
            Point[] checkPoints =
            {
                Point.Add(point, _directions[i]),
                Point.Add(point, _directions[i + 2]),
            };

            foreach (var checkPoint in checkPoints)
            {
                if (_boardService.GetCellTypeAtPoint(checkPoint) == cellTypeAtPoint)
                {
                    line.Add(checkPoint);
                }
            }

            if (line.Count > 1)
            {
                AddPoints(ref connectedPoints, line);
            }
        }
    }

    /// <summary>
    /// ������� ���������� �� ���� ��������
    /// </summary>
    private void CheckForDirectionMatch(ref List<Point> connectedPoints, Point point, CellData.CellType cellTypeAtPoint)
    {
        foreach (var direction in _directions)
        {
            var line = new List<Point>(); // �������� ����� ���������� ����� � ����� �����������

            for (int i = 1; i < 3; i++)
            {
                var checkPoint = Point.Add(point, Point.Multiply(direction, i));
                if (_boardService.GetCellTypeAtPoint(checkPoint) == cellTypeAtPoint)
                {
                    line.Add(checkPoint);
                }
            }

            if (line.Count > 1) // 2 ���������� + ����������� = ��� �� 3�� �����
            {
                AddPoints(ref connectedPoints, line);
            }
        }
    }

    /// <summary>
    /// ��������� ���������� ������ � addPoints, ����� �� �������� ���� ������ ��������� ���
    /// </summary>
    /// <param name="points">������</param>
    /// <param name="addPoints">���������� ������</param>
    public static void AddPoints(ref List<Point> points, List<Point> addPoints)
    {
        foreach (var addPoint in addPoints)
        {
            var isMatchNotFound = true;
            foreach (var point in points)
            {
                if (point.Equals(addPoint))
                {
                    isMatchNotFound = false;
                    break;
                }
            }
            if (isMatchNotFound)
            {
                points.Add(addPoint);
            }
        }
    }
}
